systemLeader = 0
leader = 0

//Get the hostname of the node
var os = require("os");
var myhostname = os.hostname();

const fs = require('fs');
nodesTxtFile = fs.readFileSync('node.js');
nodes = JSON.parse(nodesTxtFile);

//setup rabbitMQ
var amqpPub = require('amqplib/callback_api');
var amqpSub = require('amqplib/callback_api');

//subscribe to RMQ
setTimeout(subRMQMessage, 30000); // run after 30 seconds

//print the hostname
console.log(myhostname);

var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
toSend = { "hostname": myhostname, "status": "alive", "nodeID": nodeID, "isLeader": leader };

let nodesMap = new Map()

//Object data modelling library for mongo
const mongoose = require('mongoose');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/sweetShopDB?replicaSet=cfgrs';

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var stockSchema = new Schema({
  _id: Number,
  item: String,
  price: Number,
  quantity: Number
});

var stockModel = mongoose.model('Stock', stockSchema, 'stock');



app.get('/', (req, res) => {
  stockModel.find({},'item price quantity lastName', (err, stock) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(stock))
  }) 
})

app.post('/',  (req, res) => {
  var awesome_instance = new SomeModel(req.body);
  awesome_instance.save(function (err) {
  if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
  });
})

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

setTimeout(function () {
  setInterval(sendRMQMessage, 10000); // every 10 seconds
}, 20000); // run after 20 seconds

//check if leader
setTimeout(function () {
  

  //Getting the keys from nodesMap
  if (nodesMap.get(myhostname) != undefined) { 
    let keys = Array.from( nodesMap.keys() );
    //Getting the nodeIDs from nodesMap via each key
    var nodeIDs = [];
    for (i = 0; i < keys.length; i++) {
      thisNodeName = keys[i];
      nodeIDs[i] = parseInt(nodesMap.get(thisNodeName).nodeID);
    }

    //Allocating a leader
    if (parseInt(nodesMap.get(myhostname).nodeID) === Math.max(...nodeIDs)) {
      leader = 1;
      console.log(myhostname + ": I am the leader!");
      toSend = { "hostname": myhostname, "status": "alive", "nodeID": nodeID, "isLeader": leader };
    } else {
      leader = 0;
      console.log(myhostname + ": I'm not the leader.");
    }
  }

}, 60000); // run after 60 seconds

function subRMQMessage() {
  amqpSub.connect('amqp://test:test@192.168.56.104', function (error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1;
      }
      var exchange = 'logs';

      channel.assertExchange(exchange, 'fanout', {
        durable: false
      });

      channel.assertQueue('', {
        exclusive: true
      }, function (error2, q) {
        if (error2) {
          throw error2;
        }
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
        channel.bindQueue(q.queue, exchange, '');

        channel.consume(q.queue, function (msg) {
          if (msg.content) {
            console.log(" [x] %s", msg.content.toString());
          
            var message =  JSON.parse(msg.content);
            var receivedDate = new Date();
            receivedDate = receivedDate.getTime().toString();
            nodesMap.set(message.hostname, {hostname: message.hostname, status: message.status, nodeID: message.nodeID, msgReceived: receivedDate, isLeader: message.isLeader});
          }
        }, {
          noAck: true
        });
      });
    });
  });
}

function sendRMQMessage() {
  amqpPub.connect('amqp://test:test@192.168.56.104', function (error0, connection) {

    if (error0) {
      throw error0;
    }
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1;
      }
      var exchange = 'logs';
      var msg = JSON.stringify(toSend);

      channel.assertExchange(exchange, 'fanout', {
        durable: false
      });
      channel.publish(exchange, '', Buffer.from(msg));
      console.log(" [x] Sent %s", msg);
    });

    setTimeout(function () {
      connection.close();
    }, 500);
  });
}

//This is the URL endopint of your vm running docker
var url = 'http://192.168.56.104:2375';

function containerQty(){
  request.get({
    //we are using the /info url to get the base docker information
      url: url + "/info",
  }, (err, res, data) => {
      if (err) {
          console.log('Error:', err);
      } else if (res.statusCode !== 200) {
        console.log('Status:', res.statusCode);
      } else{
        //we need to parse the json response to access
          data = JSON.parse(data)
          console.log("Number of Containers = " + data.Containers);
      }
  });
}

//containerQty();

function createContainer(nodeId) {
  //create the post object to send to the docker api to create a container
  var create = {
    uri: url + "/v1.40/containers/create",
  method: 'POST',
    //deploy an alpine container that runs echo hello world
    //TODO: change Image ID to 
  json: {"Image": "cloud-coursework-project_"+nodeId, "Cmd": ["pm2-runtime", "dist.js"], "Entrypoint": "/bin/sh"}
  };


  //send the create request
  request(create, function (error, response, createBody) {
    if (!error) {
      console.log("Created container " + JSON.stringify(createBody));
    
        //post object for the container start request
        var start = {
            uri: url + "/v1.40/containers/" + createBody.Id + "/start",
          method: 'POST',
          json: {}
      };
    
      //send the start request
        request(start, function (error, response, startBody) {
          if (!error) {
            console.log("Container start completed");
      
                //post object for  wait 
                var wait = {
              uri: url + "/v1.40/containers/" + createBody.Id + "/wait",
                    method: 'POST',
                json: {}
            };
      
                
          request(wait, function (error, response, waitBody ) {
              if (!error) {
                console.log("run wait complete, container will have started");
                  
                        //send a simple get request for stdout from the container
                        request.get({
                            url: url + "/v1.40/containers/" + createBody.Id + "/logs?stdout=1",
                            }, (err, res, data) => {
                                    if (err) {
                                        console.log('Error:', err);
                                    } else if (res.statusCode !== 200) {
                                        console.log('Status:', res.statusCode);
                                    } else{
                                        //we need to parse the json response to access
                                        console.log("Container stdout = " + data);
                                        containerQty();
                                    }
                                });
                        }
            });
            }
        });

    }   
  });
}